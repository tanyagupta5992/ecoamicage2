package com.amica.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="utilities")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Utilities {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int utilityId;
	private String utilityName;
	private String defaultUnit;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY,mappedBy="utility")
	@JsonIgnore
	private List<Usage> usage=new ArrayList<>();
}
