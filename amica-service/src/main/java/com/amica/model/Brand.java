package com.amica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="brand")
@NoArgsConstructor
@Data
public class Brand {
	
	@Id
	@Column(name="brand_id")
	private int brandId;
	@Column(name="brand_cd",length = 2)
	private String brandCode;
	@Column(name="brand_desc",length = 50)
	private String brandDescription;
	

}
