package com.amica.model;

import java.util.List;

import lombok.Data;

@Data
public class Email {
	private List<String> to;
	private List<String> cc;
	private String message;
	private String subject;

}
