package com.amica.model;

import lombok.Data;

@Data
public class RecyclingDataVO {
	private String isCollaborating;
	private String collaboratingAgency;
	/*
	 * private Double recycledPlastic; private String year; private int month;
	 */
	private String hotelCode;
}
