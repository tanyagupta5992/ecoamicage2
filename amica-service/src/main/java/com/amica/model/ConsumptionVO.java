package com.amica.model;

import java.util.Map;

import lombok.Data;

@Data
public class ConsumptionVO implements Comparable<ConsumptionVO> {
	private String brand;
	private String region;
	private String country;
	private String year;
	private Double carbonConsumption;
	private Double electricityConsumption;
	private Double gasConsumption;
	private Double plasticConsumption;
	private Double waterConsumption;
	private Double carbonConsumptionAverage;
	private Double electricityConsumptionAverage;
	private Double gasConsumptionAverage;
	private Double plasticConsumptionAverage;
	private Double waterConsumptionAverage;
	private String hotelCode;
	private String variance;
	private Double carbonConsumptionLastYear;
	private String plasticVariance;
	private Double plasticConsumptionLastYear;
	private Map<String,Double> ranksAndAverages;
	@Override
	public int compareTo(ConsumptionVO o) {
		return this.getCarbonConsumption().compareTo(o.getCarbonConsumption());
	
	}
	
}
