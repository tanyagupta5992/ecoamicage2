package com.amica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="region")
@NoArgsConstructor
@Data
public class Region {
	
	@Id
	@Column(name="rgn_id")
	private int regionId;
	@Column(name="rgn_cd",length = 10)
	private String regionCode;

}
