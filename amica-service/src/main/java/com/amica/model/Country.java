package com.amica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="country")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Country {
	
	@Id
	@Column(name="cnty_id")
	private int countryId;
	@Column(name="cnty_cd",length = 3)
	private String countryCode;
	@Column(name="cnty_nm",length = 50)
	private String countryName;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "regionId")
	private Region region;

}
