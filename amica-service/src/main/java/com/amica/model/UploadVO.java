package com.amica.model;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class UploadVO {
private String hotelCode;
private int level;
private MultipartFile[] files;
private String levelType;
}
