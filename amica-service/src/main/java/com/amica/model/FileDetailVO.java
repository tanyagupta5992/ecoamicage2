package com.amica.model;

import java.util.Date;

import lombok.Data;
@Data
public class FileDetailVO {
	private String hotelCode;
	private int level;
	private String levelType;
	private int attachment_id;
	private String fileName;
	private String status;
	private String contentType;
	private Date submittedDate;
	private Date reviewedDate;
}
