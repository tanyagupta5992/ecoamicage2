package com.amica.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="measures")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Measures {
	
	@Id
	@Column(name="measure_id")
	private int measureId;
	@Column(name="O22_level1")
	private char O22Level1;
	@Column(name="E16_level1")
	private char E16Level1;
	@Column(name="E22_level1")
	private char E22Level1;
	@Column(name="E21_level2")
	private char E21Level2;
	@Column(name="O11_level2")
	private char O11Level2;
	@Column(name="O92_level2")
	private char O92Level2;
	@Column(name="E81_level3")
	private char E81Level3;
	@Column(name="E71_level3")
	private char E71Level3;
	@Column(name="W22_level3")
	private char W22Level3;	
	@JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="hotelCode")
    private Hotel hotel;
}
