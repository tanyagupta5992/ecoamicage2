package com.amica.model;

import lombok.Data;

@Data
public class HotelVO {

	private String hotelCode;
	private String propertyName;
	private String brandName;
	private String countryName;
	private String regionName;
	private String address;
	private String city;
	private String state;
	private String postalCode;
	private String propertyStatus;
	private int yearBuilt;
	private String plasticCollab;
	private String plasticCollabAgent;
}
