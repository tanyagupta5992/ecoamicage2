package com.amica.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="tips")
@NoArgsConstructor
@Data
public class HotelTips {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int tip_id;
	
	@JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="Hotel_Code")
    private Hotel hotel;
	private String tipTitle;
	@Lob
	private String tipDescription;
	private String implementedBy;
	
}
