package com.amica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="action_plans")
@Data
@NoArgsConstructor
public class ActionPlans {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@NotNull
	@Size(min=5, message="Level type should have atleast 5 characters")
	private String levelType;
	@NotNull
	@Size(min=2, message="Level info should have atleast 2 characters")
	private String levelInfo;
	@Lob
	@NotNull
	@Size(min=5, message="Description should have atleast 5 characters")
	private String description;
	@Column(name="is_custom", length=1)
	@Type(type="yes_no")
	private Boolean isCsutom;
}
