package com.amica.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "hotel")
@Data
@NoArgsConstructor
public class Hotel {

	@Id
	@Column(name = "htl_cd",length = 5)
	private String hotelCode;
	private String propertyName;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "brandId")
	private Brand brand;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "countryId")
	private Country country;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "regionId")
	private Region region;
	
	@Column(nullable = false, length = 150)
	private String address;
	@Column(nullable = false, length = 50)
	private String city;
	@Column(nullable = false, length = 50)
	private String state;
	@Column(nullable = false, length = 20)
	private String postalCode;
	@Column(nullable = false, length = 20)
	private String propertyStatus;
	@Column(nullable = false, length = 5)
	private int yearBuilt;

	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "hotel")
	private Set<HotelTips> hotelTips;
	
	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "hotel")
	private Set<Measures> measures;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY,mappedBy="hotel")
	@JsonIgnore
	private List<Usage> usage=new ArrayList<>();
	
	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "hotel")
	private Set<Attachments> attachment;
	
	private String plasticCollab;
	private String plasticCollabAgent;
}
