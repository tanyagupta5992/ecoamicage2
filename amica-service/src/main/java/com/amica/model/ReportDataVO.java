package com.amica.model;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class ReportDataVO {

	
	private String hotelCode;
	private String utilityName;
	private int utilityId;
	private String year;
	private Map<Integer,Double> usageData;
	private Map<Integer,Double> usageDataRec;
	private Double totalConsumption;
	private Double totalPlasticRecycled;
	private String uom;
}
