package com.amica.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="attachments")
@Getter
@Setter
@NoArgsConstructor
public class Attachments {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int attachment_id;
	
	@JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="Hotel_Code")
    private Hotel hotel;
	
	@Lob
	@Column(name = "FILE_BIN", nullable = false, length = 17000000)
	private byte[] file;
	
	private int level;
	private String fileName;
	private String levelType;
	//private String reviewerName;
	private String status;
	private String contentType;
	private Date submittedDate;
	private Date reviewedDate;
	

}
