package com.amica.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="usages")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Usage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer usageId;
	private String utilityType;
	private Integer month;
	private String year;
	private Double usageText;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hotelCode", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Hotel hotel;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "utilityId", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Utilities utility;

}
