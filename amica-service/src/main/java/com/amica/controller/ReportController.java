package com.amica.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amica.model.ReportDataVO;
import com.amica.service.EmissionFactorService;

@RestController
@RequestMapping("/service")
public class ReportController {
	@Autowired
	EmissionFactorService efService;
	
	@GetMapping("/getReportData/{hotelCode}/{year}/{utilityId}")
	public ReportDataVO getReportData(@PathVariable String hotelCode, @PathVariable String year, @PathVariable Integer utilityId)
	{
		List<Integer> utilityIdList = new ArrayList<Integer>();
		utilityIdList.add(utilityId);
		return efService.getReportData(hotelCode, year, utilityIdList,false);
	}
	
	@GetMapping("/getCarbonReportData/{hotelCode}/{year}")
	public ReportDataVO getCarbonReportData(@PathVariable String hotelCode, @PathVariable String year)
	{
		List<Integer> utilityIdList = new ArrayList<Integer>();
		utilityIdList.add(1);
		utilityIdList.add(2);
		return efService.getReportData(hotelCode, year, utilityIdList,true);
	}
}
