package com.amica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amica.request.RegistrationRequest;
import com.amica.service.RegistrationService;

@RestController
@RequestMapping("/service")
public class RegistrationController {
	
	@Autowired
	private RegistrationService registrationService;
	
	@PostMapping("/save/user")
	public String registerUser(@RequestBody RegistrationRequest request) {
		return registrationService.register(request);
	}
		
	
	
	
}
