package com.amica.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amica.model.ActionPlans;
import com.amica.service.ActionPlanService;

@RestController
@RequestMapping("/service")
public class ActionPlansController {
	
	@Autowired
	private ActionPlanService actionPlanService;
	
	@GetMapping("/all/plans")
	public List<ActionPlans> getAllActionPlans(){
		return actionPlanService.getAllActionPlans();
	}
	
	@PostMapping("/save/plan")
	public ResponseEntity<ActionPlans> saveActionPlan(@Valid @RequestBody ActionPlans plan) {
		return actionPlanService.saveActionPlan(plan);
	}

}
