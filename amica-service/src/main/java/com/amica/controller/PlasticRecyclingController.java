package com.amica.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amica.model.RecyclingDataVO;
import com.amica.model.ReportDataVO;
import com.amica.service.EmissionFactorService;
import com.amica.service.UsageService;

@RestController
@RequestMapping("/service")
public class PlasticRecyclingController {
	@Autowired
	UsageService usageService;
	
	@Autowired
	EmissionFactorService efService;
	
	@PostMapping("/saveRecyclingData")
	public Boolean saveRecyclingData(@RequestBody RecyclingDataVO recyclingDataVO) {
		
		return usageService.saveRecyclingData(recyclingDataVO);
	}
	
	@GetMapping("/getPlasticData/{hotelCode}/{year}")
	public ReportDataVO getPlasticData(@PathVariable String hotelCode,@PathVariable String year)
	{
		List<Integer> utilityIdList = new ArrayList<Integer>();
		utilityIdList.add(3);
		utilityIdList.add(5);
		return efService.getReportData(hotelCode, year, utilityIdList,false);
	}
	
	@GetMapping("/getPlasticCollabData/{hotelCode}")
	public RecyclingDataVO getPlasticCollabData(@PathVariable String hotelCode)
	{
		return usageService.getPlasticCollabData(hotelCode);
	}
}
