package com.amica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.amica.model.UserDetail;
import com.amica.response.AssignedPropertyProjection;
import com.amica.service.UserDetailService;

@RestController
@RequestMapping("/service")
public class UserDetailController {
	
	@Autowired
	UserDetailService detailService;
	
	@PostMapping("/save/location/{userId}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public UserDetail saveUserLocation(@PathVariable Long userId,@RequestBody UserDetail userDetail) {
		return detailService.saveUserProperty(userId, userDetail);
	}
	
	@GetMapping("get/location/{emailId}")
	public List<AssignedPropertyProjection> getPropertiesByEmail(@PathVariable String emailId){
		return detailService.findAssignedProperty(emailId);
	}

}
