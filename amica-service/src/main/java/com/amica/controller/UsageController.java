package com.amica.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.amica.model.Usage;
import com.amica.service.EmissionFactorService;
import com.amica.service.UsageService;

@RestController()
@RequestMapping("/service")
public class UsageController {

	@Autowired
	UsageService usageService;
	
	@Autowired
	EmissionFactorService efService;

	@PostMapping("/save/usage/{utilityId}/{hotelCode}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Usage saveUsageData(@PathVariable int utilityId, @PathVariable String hotelCode, @RequestBody Usage usage) {
		Usage saveUsageData = usageService.saveUsageData(utilityId, hotelCode, usage);
		return saveUsageData;
	}
	
	@PutMapping("/update/usage")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Usage> updateUsageData(@RequestBody Usage usage) {
		return usageService.updateUsageData(usage);
	}
	
	@GetMapping("/getUsage/{month}/{year}/{hotelCode}")
	public Optional<List<Usage>> findUsageData(@PathVariable Integer month,@PathVariable String year,@PathVariable String hotelCode) {
		return usageService.findUsageData(month, year, hotelCode);
	}
	
	@GetMapping("/getEmmissionFactors/{hotelCode}/{year}/{utilityId}")
	public Map<String,Double> getEmmissionFactors(@PathVariable String hotelCode, @PathVariable String year, @PathVariable Integer utilityId)
	{
		return efService.getEmmissionFactors(hotelCode, year, utilityId);
	}
}
