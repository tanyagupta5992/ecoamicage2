package com.amica.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amica.exception.NotFoundException;
import com.amica.model.Attachments;
import com.amica.model.FileDetailVO;
import com.amica.model.Measures;
import com.amica.model.UploadVO;
import com.amica.service.EmailService;
import com.amica.service.MeasuresService;
import com.amica.util.EmailUtil;

@RestController
@RequestMapping("/service")
public class MeasuresController {
	
	@Autowired
	private MeasuresService measuresService;
	
	@Autowired
	private EmailService emailService;
	
	@GetMapping("/measures/{hotelCode}")
	public Measures getMeasuresByHotel(@PathVariable String hotelCode) {
		return measuresService.findByHotel(hotelCode);
	}
	
	@PostMapping("/saveMeasures")
	public Boolean saveMeasures(@ModelAttribute UploadVO uploadVO) {
		return measuresService.saveMeasures(uploadVO);

	}
	
	@GetMapping("/getAttachments/{hotelCode}")
	public List<Attachments> getAttachments(@PathVariable String hotelCode)
	{
		return measuresService.findByHotelCode(hotelCode);
	}
	
	@PostMapping("/updateStatus/{attachmentId}/{updatedStatus}")
	public Boolean updateStatus(@PathVariable Integer attachmentId, @PathVariable String updatedStatus)
	{
		return measuresService.updateStatus(attachmentId,updatedStatus);
	}
	
	@GetMapping("/getAttachmentDetails/{hotelCode}")
	public List<FileDetailVO> getAttachmentDetails(@PathVariable String hotelCode)
	{
		return measuresService.findDetailsByHotelCode(hotelCode);
	}
	
	 @GetMapping("/downloadFile/{id}")
	    public ResponseEntity<Resource> downloadFile(@PathVariable Integer id) {
	        // Load file from database
	        Attachments file = measuresService.getFileById(id);       
	        System.out.println("Download file");
	        return ResponseEntity.ok()
	                .contentType(MediaType.parseMediaType(file.getContentType()))
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFileName() + "\"")
	                .body(new ByteArrayResource(file.getFile()));
	    }
 @GetMapping("/getFilesForReview")
 public List<FileDetailVO> getFilesForReview()
	{
		return measuresService.getAllAttachments();
	}
 
 @PutMapping("/updateMeasuresToComplete")
 public Measures updateMeasuresToComplete(@RequestBody Measures measure)
 {
	 try {
	 return measuresService.updateMeasuresToComplete(measure);
	 }
	 catch(EntityNotFoundException e)
	 {
		throw new NotFoundException("No measures found for the given ID");
	 }
 }
 
 @PostMapping("/sendEmailToReviewer/{level}/{hotelCode}")
 public String sendEmailToReviewer(@PathVariable Integer level, @PathVariable String hotelCode)
 {
	 UploadVO uploadVO = new UploadVO();
	 uploadVO.setHotelCode(hotelCode);
	 uploadVO.setLevel(level);
	 Boolean mailSent= emailService.sendMail(EmailUtil.populateEmailDetailsSendForReview(uploadVO));
	 if(mailSent)
		 return "Mail Sent Successfully";
	 return "Some error occurred while sending mail";
 }
 
	@DeleteMapping("/deleteAttachment/{attachmentId}")
	public Boolean deleteAttachment(@PathVariable Integer attachmentId) {
		try {
			return measuresService.deleteAttachment(attachmentId);
		} catch (EntityNotFoundException e) {
			throw new NotFoundException("No attachment found for the given ID");
		}
	}
}
