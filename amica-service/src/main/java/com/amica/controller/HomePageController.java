package com.amica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amica.model.HotelTips;
import com.amica.model.HotelVO;
import com.amica.service.TopTipsService;
import com.amica.service.EmissionFactorService;
import com.amica.model.ConsumptionVO;

@RestController
@RequestMapping("/service")
public class HomePageController {

	@Autowired
	private TopTipsService topTipService;
	
	@Autowired
	private EmissionFactorService efService;
	
	@GetMapping("/getAllTopTips/{hotelCode}")
	public List<HotelTips> getAllTopTips(@PathVariable String hotelCode)
	{
		return topTipService.getAllTopTips(hotelCode);
	}
	
	@GetMapping("/getConsumptionForHotelByYear/{hotelCode}/{year}")
	public ConsumptionVO getConsumptionForHotelByYear(@PathVariable String hotelCode, @PathVariable String year)
	{
		return efService.getConsumptionForHotelByYear(hotelCode,year);
	}
	
	@GetMapping("/getAverageAndRanks/{hotelCode}/{year}")
	public ConsumptionVO getAverageAndRanks(@PathVariable String hotelCode, @PathVariable String year)
	{
		return efService.getAverageAndRanks(hotelCode, year);
	}
	
	@GetMapping("getHotelDetails/{hotelCode}")
	public HotelVO getHotelDetails(@PathVariable String hotelCode)
	{
		return efService.getHotelDetails(hotelCode);
	}
	
	@GetMapping("/getRegionBrandConsumption/{hotelCode}/{year}")
	public List<ConsumptionVO> getRegionBrandConsumption(@PathVariable String hotelCode, @PathVariable String year)
	{
		return efService.getRanks(hotelCode, year);
	}
}
