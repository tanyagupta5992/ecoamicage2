package com.amica.response;

public interface AssignedPropertyProjection {
	
	String getEmail();
	String getHotelCode();

}
