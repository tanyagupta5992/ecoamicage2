package com.amica.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AssignedPropertyResponse {
	
	private String hotel;
}
