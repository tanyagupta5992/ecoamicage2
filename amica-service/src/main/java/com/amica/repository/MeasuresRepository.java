package com.amica.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.amica.model.Measures;

@Repository
public interface MeasuresRepository extends JpaRepository<Measures, Integer> {
	
	@Query(nativeQuery = true,value="SELECT * FROM amica.measures WHERE hotel_code=:hotelCode")
	Measures findByHotel(@Param("hotelCode") String hotelCode);

}
