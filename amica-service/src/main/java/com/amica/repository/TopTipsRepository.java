package com.amica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.amica.model.HotelTips;

@Repository
public interface TopTipsRepository extends JpaRepository<HotelTips, Integer>{

	//List<HotelTips> findByHotel_HotelCodeIn(List<String> hotelCodeList);

	@Query(nativeQuery = true,value = "select * from amica.tips where hotel_code=:hotelCode or hotel_code is null")
	List<HotelTips> getAllTopTips(String hotelCode);

}
