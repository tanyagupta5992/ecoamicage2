package com.amica.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.amica.model.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, Integer>{

}
