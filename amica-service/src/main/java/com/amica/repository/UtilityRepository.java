package com.amica.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.amica.model.Utilities;

@Repository
public interface UtilityRepository extends JpaRepository<Utilities, Integer> {

}
