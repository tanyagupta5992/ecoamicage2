package com.amica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.amica.model.Brand;
import com.amica.model.Country;
import com.amica.model.EmissionFactor;
import com.amica.model.Region;


@Repository
public interface EmissionFactorRepository extends JpaRepository<EmissionFactor, Integer> {
	
	List<EmissionFactor> findByRegionAndCountryAndYear(Region region, Country country,String year);

	EmissionFactor findByRegionAndCountryAndUtility_UtilityIdAndYear(Region region, Country country, Integer utilityId,
			String year);

}
