package com.amica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.amica.model.Usage;

@Repository
public interface UsageRepository extends JpaRepository<Usage, Integer> {
	
	Usage findByMonth(Integer month);
	Usage findByYear(String year);
	
	@Query(nativeQuery = true,value = "select * from amica.usages where month=?1 and year=?2 and hotel_code=?3")
	List<Usage> findUsageData(Integer month,String year,String hotelCode);
	
    List<Usage> findByYearAndHotel_HotelCode(String year, String hotelCode);	
	

	@Query(nativeQuery = true,value = "select sum(usage_text), utility_id from amica.usages where year=?1 and hotel_code=?2 group by utility_id")
	List<Object[]> findConsumption(String year,String hotelCode);
	
	@Query(nativeQuery = true,value = "select sum(usage_text), utility_id from amica.usages where year=?1 and hotel_code=?2 and month<=?3 group by utility_id")
	List<Object[]> findConsumptionYearly(String year, String hotelCode, Integer month);
	List<Usage> findByYearAndHotel_HotelCodeAndUtility_UtilityIdInOrderByMonth(String year, String hotelCode, List<Integer> utilityIdList);

}
