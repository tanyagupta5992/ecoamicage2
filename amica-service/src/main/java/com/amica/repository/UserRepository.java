package com.amica.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.amica.model.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {
	
	Optional<Users> findByEmail(String email);
	
//	@Query(nativeQuery = true,value = "SELECT user_details.hotel_code,users.email FROM "
//			+ "amica.users inner join amica.user_details on users.id=user_details.users_id where users.email=?1")
//	List<AssignedPropertyProjection> findAssignedPropertiesByEmail(String email);

}
