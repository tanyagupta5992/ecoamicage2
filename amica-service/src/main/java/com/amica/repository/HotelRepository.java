package com.amica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.amica.model.Hotel;
import com.amica.model.Region;
@Repository
public interface HotelRepository extends JpaRepository<Hotel, String>{

	List<Hotel> findByRegion(Region region);

}
