package com.amica.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.amica.model.Brand;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Integer> {

}
