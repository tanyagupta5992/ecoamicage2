package com.amica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amica.model.UserDetail;
import com.amica.response.AssignedPropertyProjection;

public interface UserDetailsRepository extends JpaRepository<UserDetail, Long> {
	
	public List<AssignedPropertyProjection> findByEmail(String email);

}
