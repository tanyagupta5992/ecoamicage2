package com.amica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.amica.model.ActionPlans;

@Repository
public interface ActionPlansRepository extends JpaRepository<ActionPlans, Integer> {

	public List<ActionPlans> findAllByOrderByLevelInfoAsc();

}
