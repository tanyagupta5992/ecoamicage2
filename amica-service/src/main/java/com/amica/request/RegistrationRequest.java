package com.amica.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode
public class RegistrationRequest {
	
	private final String firstName;
	private final String lastName;
	private final String email;
	private final String password;
	private final String appUserRole;
	private final boolean enabled;
	private final boolean locked;
}
