package com.amica.util;

import java.util.ArrayList;
import java.util.List;

import com.amica.model.Attachments;
import com.amica.model.Email;
import com.amica.model.UploadVO;

public class EmailUtil {


	public static Email populateEmailDetailsSendForReview(UploadVO uploadVO) {
		Email email = new Email();
		List<String> to = new ArrayList<String>();
		List<String> cc = new ArrayList<String>();
		String message="Dear Reviewer,\n\n";
		String subject="Level "+(uploadVO.getLevel())+(AmicaConstants.SUBJECTFORREVIEW)+(uploadVO.getHotelCode());
		to.add("sonali.nreq@gmail.com");
		cc.add("sonali.nreq@gmail.com");
		if(uploadVO.getLevelType()!=null)
		{
			message+="'Level "+(uploadVO.getLevel())+": "+uploadVO.getLevelType()+"'"+(AmicaConstants.SUBJECTFORREVIEW)+(uploadVO.getHotelCode())+".";
		}
		else
		{
			message+="Level "+(uploadVO.getLevel())+(AmicaConstants.SUBJECTFORREVIEW)+(uploadVO.getHotelCode())+".";

		}
		message+="\n\nKindly review them and take the appropriate action.\n\nRegards,\nEco-Amica Team";
		
		email.setCc(cc);
		email.setMessage(message);
		email.setSubject(subject);
		email.setTo(to);
		return email;
	}
	
	public static Email acceptOrReject(Attachments attach) {
		Email email = new Email();
		List<String> to = new ArrayList<String>();
		List<String> cc = new ArrayList<String>();
		String message="Hotel " +attach.getHotel().getHotelCode()+",\n\n";
		String subject="Level "+(attach.getLevel())+" proof "+attach.getStatus()+" by reviewer for "+(attach.getHotel().getHotelCode());
		to.add("sonali.nreq@gmail.com");
		cc.add("sonali.nreq@gmail.com");
		message+="'Level "+(attach.getLevel())+": "+attach.getLevelType()+"' proof "+attach.getStatus()+" by reviewer.\n\n";
		if(attach.getStatus().equalsIgnoreCase("REJECTED"))
		{
			message+="The upload proof option has been activated again for the rejected action item.\nKindly resubmit valid proof for approval.";
		}
		else
		{
			message+="Congratulations!! The updated status is visible on ECo-Amica.\nKeep taking steps towards greener future";
		}
		message+="\n\nRegards,\nEco-Amica Team";
		email.setCc(cc);
		email.setMessage(message);
		email.setSubject(subject);
		email.setTo(to);
		return email;
		}

}
