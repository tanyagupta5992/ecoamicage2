package com.amica.service;

import com.amica.request.RegistrationRequest;

public interface RegistrationService {

	String register(RegistrationRequest request);

}
