package com.amica.service;

import java.util.List;

import com.amica.model.Attachments;
import com.amica.model.FileDetailVO;
import com.amica.model.Measures;
import com.amica.model.UploadVO;

public interface MeasuresService {
	
	Measures findByHotel(String hotelCode);

	Boolean saveMeasures(UploadVO uploadVO);

	List<Attachments> findByHotelCode(String hotelCode);

	Boolean updateStatus(Integer attachmentId, String updatedStatus);

	List<FileDetailVO> findDetailsByHotelCode(String hotelCode);

	Attachments getFileById(Integer Id);

	List<FileDetailVO> getAllAttachments();

	Measures updateMeasuresToComplete(Measures measure);

	Boolean deleteAttachment(Integer attachmentId);

}
