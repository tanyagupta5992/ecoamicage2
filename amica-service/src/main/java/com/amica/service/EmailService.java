package com.amica.service;

import com.amica.model.Email;

public interface EmailService {
 public Boolean sendMail(Email email);
}
