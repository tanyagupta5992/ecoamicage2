package com.amica.service;

import java.util.List;
import java.util.Map;

import com.amica.model.ConsumptionVO;
import com.amica.model.Hotel;
import com.amica.model.HotelVO;
import com.amica.model.ReportDataVO;

public interface EmissionFactorService {
	
	ConsumptionVO getConsumptionForHotelByYear(String hotelCode, String year);
	
	public ConsumptionVO getAverageAndRanks(String hotelCode, String year);

	Map<String,Double> getEmmissionFactors(String hotelCode, String year, Integer utilityId);

	ReportDataVO getReportData(String hotelCode, String year, List<Integer> utilityIdList,Boolean isCarbonReport);

	HotelVO getHotelDetails(String hotelCode);

	List<ConsumptionVO> getRanks(String hotelCode, String year);
}
