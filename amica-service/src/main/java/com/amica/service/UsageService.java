package com.amica.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.amica.model.RecyclingDataVO;
import com.amica.model.Usage;

public interface UsageService {
	
	public Usage saveUsageData(int utilityId,String hotelCode,Usage usage);
	
	public Usage findByMonth(Integer month);
	public Usage findByYear(String year);
	
	public ResponseEntity<Usage> updateUsageData(Usage usage);
	
	public Optional<List<Usage>> findUsageData(Integer month,String year,String hotelCode);
	
	public List<Usage> findByYearAndHotel_HotelCode(String year, String hotelCode);

	public List<Object[]> findConsumption(String year, String hotelCode);
	
	public List<Object[]> findConsumption(String year, String hotelCode, Integer month);

	public Boolean saveRecyclingData(RecyclingDataVO recyclingDataVO);

	public List<Usage> findByYearAndHotel_HotelCodeAndUtility_UtilityIdInOrderByMonth(String year, String hotelCode,
			List<Integer> utilityIdList);

	public RecyclingDataVO getPlasticCollabData(String hotelCode);


}
