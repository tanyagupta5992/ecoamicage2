package com.amica.service;

import java.util.List;

import com.amica.model.UserDetail;
import com.amica.response.AssignedPropertyProjection;

public interface UserDetailService {
	
	UserDetail saveUserProperty(long userId,UserDetail userDetail);
	
	List<AssignedPropertyProjection> findAssignedProperty(String email);
}
