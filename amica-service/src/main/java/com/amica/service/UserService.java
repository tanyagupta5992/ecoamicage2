package com.amica.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.amica.model.Users;

public interface UserService extends UserDetailsService {
	
	UserDetails loadUserByUsername(String email);
	
	public String signUpUser(Users user);
	
//	public List<AssignedPropertyProjection> getPropertiesByEmail(String emailId);

}
