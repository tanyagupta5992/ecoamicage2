package com.amica.service;

import java.util.List;

import com.amica.model.HotelTips;

public interface TopTipsService {

	List<HotelTips> getAllTopTips(String hotelCode);

}
