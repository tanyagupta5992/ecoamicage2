package com.amica.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.amica.model.ActionPlans;

public interface ActionPlanService {
	
	public List<ActionPlans> getAllActionPlans();
	
	public ResponseEntity<ActionPlans> saveActionPlan(ActionPlans plan);

}
