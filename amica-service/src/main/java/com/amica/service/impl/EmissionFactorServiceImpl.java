package com.amica.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amica.model.ConsumptionVO;
import com.amica.model.Country;
import com.amica.service.EmissionFactorService;
import com.amica.exception.NotFoundException;
import com.amica.model.Brand;
import com.amica.model.EmissionFactor;
import com.amica.model.Hotel;
import com.amica.model.HotelVO;
import com.amica.model.Region;
import com.amica.model.ReportDataVO;
import com.amica.model.Usage;
import com.amica.model.Utilities;
import com.amica.repository.EmissionFactorRepository;
import com.amica.repository.HotelRepository;
import com.amica.repository.UtilityRepository;
import com.amica.service.UsageService;

@Service
public class EmissionFactorServiceImpl implements EmissionFactorService {

	@Autowired
	private UsageService usageService;

	@Autowired
	private EmissionFactorRepository efRepository;

	@Autowired
	private HotelRepository hotelRepository;
	
	@Autowired
	private UtilityRepository utilRepo;

	@Override
	public ConsumptionVO getConsumptionForHotelByYear(String hotelCode, String year) {
		String varianceStr = "N/A";
		String plasticVarianceStr = "N/A";
		Hotel hotel = hotelRepository.getOne(hotelCode);
		Calendar c = Calendar.getInstance();
		Integer currentMonth=c.get(Calendar.MONTH);
		Integer currentYear=c.get(Calendar.YEAR);
		if(!(year.equalsIgnoreCase(currentYear.toString())))
		{
			currentMonth=11;
		}
		ConsumptionVO consumptionVOCurrentYear = getConsumptionVODetails(hotel.getRegion(), hotel.getCountry(), year,
				hotelCode,currentMonth);
		Integer lastYear = (new Integer(year)) - 1;
		ConsumptionVO consumptionVOLastYear = getConsumptionVODetails(hotel.getRegion(), hotel.getCountry(),
				lastYear.toString(), hotelCode,currentMonth);
		Double carbonConsumptionCurrent = consumptionVOCurrentYear.getCarbonConsumption();
		Double carbonConsumptionPrevious = consumptionVOLastYear.getCarbonConsumption();
		if (carbonConsumptionPrevious != 0) {
			varianceStr = calculateVariance(carbonConsumptionCurrent, carbonConsumptionPrevious);
		}
		consumptionVOCurrentYear.setVariance(varianceStr);
		if ((consumptionVOLastYear.getPlasticConsumption() != null)
				&& (consumptionVOLastYear.getPlasticConsumption() != 0))
			plasticVarianceStr = calculateVariance(consumptionVOCurrentYear.getPlasticConsumption(),
					consumptionVOLastYear.getPlasticConsumption());
		consumptionVOCurrentYear.setPlasticVariance(plasticVarianceStr);
		consumptionVOCurrentYear.setCarbonConsumptionLastYear(consumptionVOLastYear.getCarbonConsumption());
		consumptionVOCurrentYear.setPlasticConsumptionLastYear(consumptionVOLastYear.getPlasticConsumption());

		consumptionVOCurrentYear.setHotelCode(hotelCode);
		consumptionVOCurrentYear.setRegion(hotel.getRegion().getRegionCode());
		consumptionVOCurrentYear.setBrand(hotel.getBrand().getBrandDescription());
		consumptionVOCurrentYear.setYear(year);
		return consumptionVOCurrentYear;
	}

	private ConsumptionVO getConsumptionVODetails(Region region, Country country, String year, String hotelCode,Integer currentMonth) {
		ConsumptionVO consumptionVO = new ConsumptionVO();
		Double emissionFactorElectricity = null;
		Double emissionFactorGas = null;
		Double carbonConsumption = 0.0;
		List<EmissionFactor> emissionFactor = efRepository.findByRegionAndCountryAndYear(region, country, year);
		for (EmissionFactor ef : emissionFactor) {
			switch (ef.getUtility().getUtilityId()) {
			case 1:
				emissionFactorElectricity = ef.getEmissionFactorVal();
				break;
			case 2:
				emissionFactorGas = ef.getEmissionFactorVal();
				break;
			}
		}
		List<Object[]> consumption = usageService.findConsumption(year, hotelCode,currentMonth);
		for (Object[] cons : consumption) {
			switch ((int) cons[1]) {
			case 1:
				consumptionVO.setElectricityConsumption(new Double((cons[0]).toString()));
				if (emissionFactorElectricity != null)
					carbonConsumption += (consumptionVO.getElectricityConsumption()) * emissionFactorElectricity;
				break;
			case 2:
				consumptionVO.setGasConsumption(new Double((cons[0]).toString()));
				if (emissionFactorGas != null)
					carbonConsumption += (consumptionVO.getGasConsumption()) * emissionFactorGas;
				break;
			case 3:
				consumptionVO.setPlasticConsumption(new Double((cons[0]).toString()));
				break;
			case 4:
				consumptionVO.setWaterConsumption(new Double((cons[0]).toString()));
				break;
			}
		}
		consumptionVO.setCarbonConsumption(carbonConsumption);
		return consumptionVO;
	}

	private String calculateVariance(Double current, Double previous) {
		String varianceStr = "";
		Double variance = ((current - previous) / previous) * 100;
		varianceStr = variance.toString();
		return varianceStr;
	}
	
	private ConsumptionVO getConsumptionForHotel(Hotel hotel, String year) {
		int numberOfMonths=12;
		ConsumptionVO consumptionVOCurrentYear = getConsumptionVODetails(hotel.getRegion(), hotel.getCountry(), year,
				hotel.getHotelCode(),11);
		consumptionVOCurrentYear.setHotelCode(hotel.getHotelCode());
		consumptionVOCurrentYear.setRegion(hotel.getRegion().getRegionCode());
		consumptionVOCurrentYear.setBrand(hotel.getBrand().getBrandDescription());
		consumptionVOCurrentYear.setYear(year);
		consumptionVOCurrentYear.setCountry(hotel.getCountry().getCountryName());;
		if (consumptionVOCurrentYear.getCarbonConsumption() != null) {
			consumptionVOCurrentYear
					.setCarbonConsumptionAverage((consumptionVOCurrentYear.getCarbonConsumption()) / numberOfMonths);
		} else {
			consumptionVOCurrentYear.setCarbonConsumptionAverage(0.0);
		}
		if (consumptionVOCurrentYear.getElectricityConsumption() != null) {
			consumptionVOCurrentYear.setElectricityConsumptionAverage(
					(consumptionVOCurrentYear.getElectricityConsumption()) / numberOfMonths);
		} else {
			consumptionVOCurrentYear.setElectricityConsumptionAverage(0.0);
		}
		if (consumptionVOCurrentYear.getGasConsumption() != null) {
			consumptionVOCurrentYear
					.setGasConsumptionAverage((consumptionVOCurrentYear.getGasConsumption()) / numberOfMonths);
		} else {
			consumptionVOCurrentYear.setGasConsumptionAverage(0.0);
		}
		return consumptionVOCurrentYear;
	}
	
	public ConsumptionVO getAverageAndRanks(String hotelCode, String year) {
		Hotel hotelCurrent=hotelRepository.getOne(hotelCode);
		ConsumptionVO conumptionVO = getConsumptionForHotel(hotelCurrent,year);
		Double currentRankInRegion=1.0;
		Double regionAverage=(conumptionVO.getCarbonConsumptionAverage() !=null) ? conumptionVO.getCarbonConsumptionAverage() :0.0;
		Double currentRankInRegionElec=1.0;
		Double regionAverageElec=(conumptionVO.getElectricityConsumptionAverage() !=null) ? conumptionVO.getElectricityConsumptionAverage():0.0;
		Double currentRankInRegionGas=1.0;
		Double regionAverageGas=(conumptionVO.getGasConsumptionAverage() !=null) ? conumptionVO.getGasConsumptionAverage() :0.0;
		List<Hotel> hotelList = hotelRepository.findByRegion(hotelCurrent.getRegion());
		int noOfHotels=hotelList.size();
		Map<String,Double> ranksAndAverages = new HashMap<String,Double>();
		for(Hotel h :hotelList)
		{
			if (!(h.getHotelCode().equalsIgnoreCase(hotelCode))) {
				ConsumptionVO consumptionVOTemp = getConsumptionForHotel(h, year);
				regionAverage += consumptionVOTemp.getCarbonConsumptionAverage();
				if (consumptionVOTemp.getCarbonConsumptionAverage() < conumptionVO.getCarbonConsumptionAverage()) {
					currentRankInRegion++;
				}

				regionAverageElec += consumptionVOTemp.getElectricityConsumptionAverage();
				if (consumptionVOTemp.getElectricityConsumptionAverage() < conumptionVO
						.getElectricityConsumptionAverage()) {
					currentRankInRegionElec++;
				}
				regionAverageGas += consumptionVOTemp.getGasConsumptionAverage();
				if (consumptionVOTemp.getGasConsumptionAverage() < conumptionVO.getGasConsumptionAverage()) {
					currentRankInRegionGas++;
				}

			}
		}
		regionAverage/=noOfHotels;
		regionAverageElec/=noOfHotels;
		regionAverageGas/=noOfHotels;
		ranksAndAverages.put("Carbon Consumption Region Rank", currentRankInRegion);
		ranksAndAverages.put("Carbon Consumption Region Average", regionAverage);
		ranksAndAverages.put("Electricity Consumption Region Rank", currentRankInRegionElec);
		ranksAndAverages.put("Electricity Consumption Region Average", regionAverageElec);
		ranksAndAverages.put("Gas Consumption Region Rank", currentRankInRegionGas);
		ranksAndAverages.put("Gas Consumption Region Average", regionAverageGas);
		conumptionVO.setRanksAndAverages(ranksAndAverages);
		
		return conumptionVO;
	}

	@Override
	public Map<String,Double> getEmmissionFactors(String hotelCode, String year, Integer utilityId) {
		// TODO Auto-generated method stub
		Map<String,Double> efv= new HashMap<String,Double>();
		Double eVal=null;
		Hotel h = hotelRepository.getOne(hotelCode);
		EmissionFactor ef= efRepository.findByRegionAndCountryAndUtility_UtilityIdAndYear(h.getRegion(),h.getCountry(),utilityId,year);
		if(ef!=null)
		{
			eVal= ef.getEmissionFactorVal();
		}
		efv.put("EmissionFactorVal", eVal);
		return efv;
	}

	@Override
	public ReportDataVO getReportData(String hotelCode, String year, List<Integer> utilityIdList, Boolean isCarbonReport) {
		// TODO Auto-generated method stub
		 Utilities utility = utilRepo.getOne(utilityIdList.get(0));
		ReportDataVO reportDataVO = new ReportDataVO();
		reportDataVO.setHotelCode(hotelCode);
		reportDataVO.setUtilityId(utility.getUtilityId());
		reportDataVO.setUtilityName(utility.getUtilityName());
		reportDataVO.setUom(utility.getDefaultUnit());
		reportDataVO.setYear(year);
		Map<Integer, Double> usageByMonth = new HashMap<Integer, Double>();
		Map<Integer, Double> usageByMonthRec = new HashMap<Integer, Double>();
		Double totalConsumption = 0.0;
		Double totalRecycled = 0.0;
		Double emissionFactorElectricity = 0.0;
		Double emissionFactorGas = 0.0;
		if(isCarbonReport)
		{
			Hotel h = hotelRepository.getOne(hotelCode);
			
			List<EmissionFactor> emissionFactor = efRepository.findByRegionAndCountryAndYear(h.getRegion(), h.getCountry(), year);
			for (EmissionFactor ef : emissionFactor) {
				switch (ef.getUtility().getUtilityId()) {
				case 1:
					emissionFactorElectricity = ef.getEmissionFactorVal();
					break;
				case 2:
					emissionFactorGas = ef.getEmissionFactorVal();
					break;
				}
			}
			reportDataVO.setUom("KgCo2");
		}
		List<Usage> usageList = usageService.findByYearAndHotel_HotelCodeAndUtility_UtilityIdInOrderByMonth(year,
				hotelCode, utilityIdList);
		for (Usage u : usageList) {
			Integer month = u.getMonth();
			Double usageTxt = u.getUsageText();
			if(isCarbonReport)
			{if(usageTxt!=null) {
				if(u.getUtility().getUtilityId()==1 )
				{
					usageTxt*= emissionFactorElectricity;
				}
				else if(u.getUtility().getUtilityId()==2)
				{
					usageTxt*= emissionFactorGas;
				}
			}
			}
			if ((u.getUtility().getUtilityId()) == 5) {
				usageByMonthRec.put(month, usageTxt);
				if (usageTxt != null) {
					totalRecycled += usageTxt;

				}
			} else {
				if( isCarbonReport && (usageByMonth.containsKey(month)) )
				{
					Double usageTxtTemp=usageTxt+(usageByMonth.get(month));
					usageByMonth.put(month, usageTxtTemp);
				}
				else {
				usageByMonth.put(month, usageTxt);}
				if (usageTxt != null) {
					totalConsumption += usageTxt;

				}
			}
		}
		reportDataVO.setTotalConsumption(totalConsumption);
		reportDataVO.setTotalPlasticRecycled(totalRecycled);
		reportDataVO.setUsageData(usageByMonth);
		reportDataVO.setUsageDataRec(usageByMonthRec);
		return reportDataVO;
	}

	@Override
	public HotelVO getHotelDetails(String hotelCode) {
		// TODO Auto-generated method stub
		Hotel h= hotelRepository.getOne(hotelCode);
		if(h==null)
			throw new NotFoundException("Hotel not found " + hotelCode);
		HotelVO hVO= new HotelVO();
		hVO.setAddress(h.getAddress());
		hVO.setBrandName(h.getBrand().getBrandDescription());
		hVO.setCity(h.getCity());
		hVO.setCountryName(h.getCountry().getCountryName());
		hVO.setHotelCode(hotelCode);
		hVO.setPlasticCollab(h.getPlasticCollab());
		hVO.setPlasticCollabAgent(h.getPlasticCollabAgent());
		hVO.setPostalCode(h.getPostalCode());
		hVO.setPropertyName(h.getPropertyName());
		hVO.setPropertyStatus(h.getPropertyStatus());
		hVO.setRegionName(h.getRegion().getRegionCode());
		hVO.setState(h.getState());
		hVO.setYearBuilt(h.getYearBuilt());
		return hVO;
	}
	
	public List<ConsumptionVO> getRanks(String hotelCode, String year) {
		List<ConsumptionVO> consVOList= new ArrayList<ConsumptionVO>();
		Hotel hotelCurrent=hotelRepository.getOne(hotelCode);
		ConsumptionVO conumptionVO = getConsumptionForHotel(hotelCurrent,year);
		consVOList.add(conumptionVO);
		Double currentRankInRegion=1.0;
		Double regionAverage=(conumptionVO.getCarbonConsumptionAverage() !=null) ? conumptionVO.getCarbonConsumptionAverage() :0.0;
		Double currentRankInRegionElec=1.0;
		Double regionAverageElec=(conumptionVO.getElectricityConsumptionAverage() !=null) ? conumptionVO.getElectricityConsumptionAverage():0.0;
		Double currentRankInRegionGas=1.0;
		Double regionAverageGas=(conumptionVO.getGasConsumptionAverage() !=null) ? conumptionVO.getGasConsumptionAverage() :0.0;
		List<Hotel> hotelList = hotelRepository.findByRegion(hotelCurrent.getRegion());
		int noOfHotels=hotelList.size();
		Map<String,Double> ranksAndAverages = new HashMap<String,Double>();
		for(Hotel h :hotelList)
		{
			if (!(h.getHotelCode().equalsIgnoreCase(hotelCode))) {
				ConsumptionVO consumptionVOTemp = getConsumptionForHotel(h, year);
				consVOList.add(consumptionVOTemp);
				regionAverage += consumptionVOTemp.getCarbonConsumptionAverage();
				if (consumptionVOTemp.getCarbonConsumptionAverage() < conumptionVO.getCarbonConsumptionAverage()) {
					currentRankInRegion++;
				}

				regionAverageElec += consumptionVOTemp.getElectricityConsumptionAverage();
				if (consumptionVOTemp.getElectricityConsumptionAverage() < conumptionVO
						.getElectricityConsumptionAverage()) {
					currentRankInRegionElec++;
				}
				regionAverageGas += consumptionVOTemp.getGasConsumptionAverage();
				if (consumptionVOTemp.getGasConsumptionAverage() < conumptionVO.getGasConsumptionAverage()) {
					currentRankInRegionGas++;
				}

			}
		}
		regionAverage/=noOfHotels;
		regionAverageElec/=noOfHotels;
		regionAverageGas/=noOfHotels;
		ranksAndAverages.put("Carbon Consumption Region Rank", currentRankInRegion);
		ranksAndAverages.put("Carbon Consumption Region Average", regionAverage);
		ranksAndAverages.put("Electricity Consumption Region Rank", currentRankInRegionElec);
		ranksAndAverages.put("Electricity Consumption Region Average", regionAverageElec);
		ranksAndAverages.put("Gas Consumption Region Rank", currentRankInRegionGas);
		ranksAndAverages.put("Gas Consumption Region Average", regionAverageGas);
		conumptionVO.setRanksAndAverages(ranksAndAverages);
		Collections.sort(consVOList);
		return consVOList;
	}
	

}
