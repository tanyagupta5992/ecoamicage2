package com.amica.service.impl;

import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import com.amica.model.Email;
import com.amica.service.EmailService;
import com.amica.util.AmicaConstants;

@Service
public class EmailServiceImpl implements EmailService{

	@Override
	public Boolean sendMail(Email email) {
		System.out.println("sending out mail");
		List<String> to = email.getTo();
		List<String> cc = email.getCc();
		String message=email.getMessage();
		String subject=email.getSubject();
		String from = AmicaConstants.FROM;
		boolean f = false;
		String password=AmicaConstants.PASSWORD;
		String host = "smtp.gmail.com";
		// get properties
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", "465");
		properties.put("mail.smtp.ssl.enable", true);
		properties.put("mail.smtp.auth", true);

		Session session = Session.getDefaultInstance(properties, new Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}

		});

		session.setDebug(true);

		MimeMessage mimeMessage = new MimeMessage(session);

		try {
			mimeMessage.setFrom(from);
			for(String recepient : to)
			{
			mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
			}
			for(String recepient : cc) {
			mimeMessage.addRecipient(Message.RecipientType.CC, new InternetAddress(recepient));
			}
			mimeMessage.setSubject(subject);
			mimeMessage.setText(message);
//			mimeMessage.setHeader("X-Priority", "1");
			mimeMessage.addHeader("X-Priority", "1");

			Transport.send(mimeMessage);
			System.out.println("Send successfully");
			f = true;
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return f;
	}

}
