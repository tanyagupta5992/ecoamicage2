package com.amica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.amica.model.ActionPlans;
import com.amica.repository.ActionPlansRepository;
import com.amica.service.ActionPlanService;

@Service
public class ActionPlanServiceImpl implements ActionPlanService{
	
	@Autowired
	private ActionPlansRepository actionPlanRepository;

	@Override
	public List<ActionPlans> getAllActionPlans() {
		return actionPlanRepository.findAllByOrderByLevelInfoAsc();
	}

	@Override
	public ResponseEntity<ActionPlans> saveActionPlan(ActionPlans plan) {
		if(plan.getLevelInfo().isEmpty() && plan.getLevelType().isEmpty() && plan.getDescription().isEmpty()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		plan.setIsCsutom(true);
		return new ResponseEntity<>(actionPlanRepository.save(plan), HttpStatus.CREATED);
	}

}
