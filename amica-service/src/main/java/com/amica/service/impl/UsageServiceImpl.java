package com.amica.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.amica.exception.NotFoundException;
import com.amica.model.Hotel;
import com.amica.model.RecyclingDataVO;
import com.amica.model.Usage;
import com.amica.model.Utilities;
import com.amica.repository.HotelRepository;
import com.amica.repository.UsageRepository;
import com.amica.repository.UtilityRepository;
import com.amica.service.UsageService;

@Service
public class UsageServiceImpl implements UsageService {

	@Autowired
	private HotelRepository hotelRepo;

	@Autowired
	private UtilityRepository utilRepo;

	@Autowired
	private UsageRepository usageRepo;

	@Override
	@Transactional
	public Usage saveUsageData(int utilityId, String hotelCode, Usage usage) {

		List<Usage> usageData = new ArrayList<>();
		Hotel hotel = new Hotel();
		Utilities utility = new Utilities();
		Optional<Utilities> findUtilId = utilRepo.findById(utilityId);
		if (!findUtilId.isPresent()) {
			throw new NotFoundException("Utility id not found " + utilityId);
		}
		Utilities utilities = findUtilId.get();
		usage.setUtility(utilities);
		Optional<Hotel> findHotelCode = hotelRepo.findById(hotelCode);
		if (!findHotelCode.isPresent()) {
			throw new NotFoundException("Hotel code not found " + hotelCode);
		}
		Hotel hotel2 = findHotelCode.get();
		usage.setHotel(hotel2);

		Usage saveUsageData = usageRepo.save(usage);
		usageData.add(saveUsageData);
		hotel.setUsage(usageData);
		utility.setUsage(usageData);

		return saveUsageData;
	}

	@Override
	public Usage findByMonth(Integer month) {
		return usageRepo.findByMonth(month);
	}

	@Override
	public Usage findByYear(String year) {
		return usageRepo.findByYear(year);
	}

	@Override
	public ResponseEntity<Usage> updateUsageData(Usage usage) {
		Optional<Usage> findByMonth = Optional.of(usageRepo.getOne(usage.getUsageId()));
		if (findByMonth.isPresent()) {
			Usage data = findByMonth.get();
			System.out.println(usage.getUsageText());
			data.setUsageText(usage.getUsageText());
			return new ResponseEntity<>(usageRepo.save(data), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public Optional<List<Usage>> findUsageData(Integer month, String year, String hotelCode) {
		Optional<List<Usage>> findUsageData = Optional.ofNullable(usageRepo.findUsageData(month, year, hotelCode));
		if(!findUsageData.isPresent()) {
			throw new NotFoundException("Usage data not found for " + hotelCode + " property, " +"for " + month + " month " + "and " + year + " year.");
		}
		return findUsageData;
	}

	@Override
	public List<Usage> findByYearAndHotel_HotelCode(String year, String hotelCode) {
		// TODO Auto-generated method stub
		return usageRepo.findByYearAndHotel_HotelCode(year,hotelCode);
	}

	@Override
	public List<Object[]> findConsumption(String year, String hotelCode) {
		// TODO Auto-generated method stub
		return usageRepo.findConsumption(year, hotelCode);
	}

	@Override
	public List<Object[]> findConsumption(String year, String hotelCode, Integer month) {
		// TODO Auto-generated method stub
		return usageRepo.findConsumptionYearly(year, hotelCode,month);
	}

	@Override
	public Boolean saveRecyclingData(RecyclingDataVO recyclingDataVO) {
		// TODO Auto-generated method stub
		Hotel h = hotelRepo.getOne(recyclingDataVO.getHotelCode());
		h.setPlasticCollab(recyclingDataVO.getIsCollaborating());
		h.setPlasticCollabAgent(recyclingDataVO.getCollaboratingAgency());
		hotelRepo.save(h);
		return null;
	}

	@Override
	public List<Usage> findByYearAndHotel_HotelCodeAndUtility_UtilityIdInOrderByMonth(String year, String hotelCode,
			List<Integer> utilityIdList) {
		// TODO Auto-generated method stub
		return usageRepo.findByYearAndHotel_HotelCodeAndUtility_UtilityIdInOrderByMonth(year, hotelCode, utilityIdList);
	}

	@Override
	public RecyclingDataVO getPlasticCollabData(String hotelCode) {
		// TODO Auto-generated method stub
		Hotel h=  hotelRepo.getOne(hotelCode);
		RecyclingDataVO recyclingDataVO = new RecyclingDataVO();
		recyclingDataVO.setCollaboratingAgency(h.getPlasticCollabAgent());
		recyclingDataVO.setIsCollaborating(h.getPlasticCollab());
		recyclingDataVO.setHotelCode(hotelCode);
		return recyclingDataVO;
	}


}
