package com.amica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amica.model.HotelTips;
import com.amica.repository.TopTipsRepository;
import com.amica.service.TopTipsService;

@Service
public class TopTipsServiceImpl implements TopTipsService{

	@Autowired
	TopTipsRepository topTipsRepository;
	
	@Override
	public List<HotelTips> getAllTopTips(String hotelCode) {
		// TODO Auto-generated method stub
		return topTipsRepository.getAllTopTips(hotelCode);
	}

}
