package com.amica.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.amica.model.Users;
import com.amica.repository.UserRepository;
import com.amica.service.UserService;
import com.amica.util.AmicaConstants;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public UserDetails loadUserByUsername(String email) {
		return userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException(
				String.format(AmicaConstants.USER_NOT_FOUND,email)));
	}
	
	@Transactional
	public String signUpUser(Users user) {
		boolean userExists = userRepository.findByEmail(user.getEmail()).isPresent();
		if (userExists) {
			throw new IllegalStateException("user already registered");
		}

		String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		userRepository.save(user);
		return "User registered succesfully";
	}

//	@Override
//	public List<AssignedPropertyProjection> getPropertiesByEmail(String emailId) {
////		List<AssignedPropertyProjection> findAssignedPropertiesByEmail = userDetails.findAssignedPropertiesByEmailDetails(emailId);
//		List<AssignedPropertyResponse> response=new ArrayList<AssignedPropertyResponse>();
//		boolean userExists=findAssignedPropertiesByEmail.isEmpty();
//		if(userExists) {
//			throw new NotFoundException("Not found assigned properties for user " + emailId);
//		}
//		for(AssignedPropertyProjection temp : findAssignedPropertiesByEmail) {
//			AssignedPropertyResponse assigned=new AssignedPropertyResponse();
//			assigned.setHotel(temp.getHotelCode());
//			log.info(temp.getEmail());
//		}
//		
//		return findAssignedPropertiesByEmail;
//	}
}



