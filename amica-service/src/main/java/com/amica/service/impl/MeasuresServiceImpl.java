package com.amica.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amica.exception.NotFoundException;
import com.amica.model.Attachments;
import com.amica.model.FileDetailVO;
import com.amica.model.Hotel;
import com.amica.model.Measures;
import com.amica.model.UploadVO;
import com.amica.repository.AttachmentsRepository;
import com.amica.repository.HotelRepository;
import com.amica.repository.MeasuresRepository;
import com.amica.service.EmailService;
import com.amica.service.MeasuresService;
import com.amica.util.EmailUtil;

@Service
public class MeasuresServiceImpl implements MeasuresService {
	
	@Autowired
	private MeasuresRepository measuresRepository;
	
	@Autowired
	private AttachmentsRepository attachmentsRepository;
	
	@Autowired
	private HotelRepository hotelRepository;
	
	@Autowired
	private EmailService emailService;

	@Override
	public Measures findByHotel(String hotelCode) {
		
		Measures findByHotel = measuresRepository.findByHotel(hotelCode);
		if(findByHotel == null) {
			throw new NotFoundException("Hotel data not found : " + hotelCode);
		}		
		return findByHotel;
	}

	@Override
	public Boolean saveMeasures(UploadVO uploadVO) {
		MultipartFile[] files = uploadVO.getFiles();
		if ((files == null) || (files.length <= 0)) {
			return false;
		}
		// indicate for review logic here

		Hotel hotel = hotelRepository.getOne(uploadVO.getHotelCode());
		int level = uploadVO.getLevel();
		String levelType =uploadVO.getLevelType();
		List<Attachments> attachmentsList = new ArrayList<Attachments>();
		for (MultipartFile file : files) {
			Attachments attach = new Attachments();
			try {
				attach.setFile(file.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			attach.setHotel(hotel);
			attach.setLevel(level);
			attach.setFileName(file.getOriginalFilename());
			attach.setStatus("Submitted");
			attach.setContentType(file.getContentType());
			attach.setLevelType(levelType);
			attach.setSubmittedDate(new Date());
			attachmentsList.add(attach);
		}
		attachmentsRepository.saveAll(attachmentsList);
		emailService.sendMail(EmailUtil.populateEmailDetailsSendForReview(uploadVO));
		return true;
	}

	@Override
	public List<Attachments> findByHotelCode(String hotelCode) {
		
		return attachmentsRepository.findByHotel_HotelCode(hotelCode);
	}

	@Override
	public Boolean updateStatus(Integer attachmentId, String updatedStatus) {
		Attachments attach = attachmentsRepository.getOne(attachmentId);
		if(attach==null)
		return false;
		attach.setStatus(updatedStatus);
		attach.setReviewedDate(new Date());
		attachmentsRepository.save(attach);
		emailService.sendMail(EmailUtil.acceptOrReject(attach));
		return true;
	}

	@Override
	public List<FileDetailVO> findDetailsByHotelCode(String hotelCode) {
		List<FileDetailVO> fileDetailVOList = new ArrayList<FileDetailVO>();
		List<Attachments> attachmentList= attachmentsRepository.findByHotel_HotelCode(hotelCode);
		for(Attachments attach : attachmentList)
		{
			FileDetailVO fileDetailVO= new FileDetailVO();
			fileDetailVO.setAttachment_id(attach.getAttachment_id());
			fileDetailVO.setContentType(attach.getContentType());
			fileDetailVO.setFileName(attach.getFileName());
			fileDetailVO.setHotelCode(hotelCode);
			fileDetailVO.setLevel(attach.getLevel());
			fileDetailVO.setLevelType(attach.getLevelType());
			fileDetailVO.setStatus(attach.getStatus());
			fileDetailVO.setSubmittedDate(attach.getSubmittedDate());
			fileDetailVO.setReviewedDate(attach.getReviewedDate());
			fileDetailVOList.add(fileDetailVO);
		}
		return fileDetailVOList;
	}

	@Override
	public Attachments getFileById(Integer Id) {
		// TODO Auto-generated method stub
		return attachmentsRepository.getOne(Id);
	}

	@Override
	public List<FileDetailVO> getAllAttachments() {
		// TODO Auto-generated method stub
		List<FileDetailVO> fileDetailVOList = new ArrayList<FileDetailVO>();
		List<Attachments> attachmentList= attachmentsRepository.findAll();
		for(Attachments attach : attachmentList)
		{
			FileDetailVO fileDetailVO= new FileDetailVO();
			fileDetailVO.setAttachment_id(attach.getAttachment_id());
			fileDetailVO.setContentType(attach.getContentType());
			fileDetailVO.setFileName(attach.getFileName());
			fileDetailVO.setHotelCode(attach.getHotel().getHotelCode());
			fileDetailVO.setLevel(attach.getLevel());
			fileDetailVO.setLevelType(attach.getLevelType());
			fileDetailVO.setStatus(attach.getStatus());
			fileDetailVO.setSubmittedDate(attach.getSubmittedDate());
			fileDetailVO.setReviewedDate(attach.getReviewedDate());
			fileDetailVOList.add(fileDetailVO);
		}
		return fileDetailVOList;
	}

	@Override
	public Measures updateMeasuresToComplete(Measures measure) {
		Measures m=measuresRepository.getOne(measure.getMeasureId());
		m.setE16Level1(measure.getE16Level1());
		m.setE21Level2(measure.getE21Level2());
		m.setE22Level1(measure.getE22Level1());
		m.setE71Level3(measure.getE71Level3());
		m.setE81Level3(measure.getE81Level3());
		m.setO11Level2(measure.getO11Level2());
		m.setO22Level1(measure.getO22Level1());
		m.setO92Level2(measure.getO92Level2());
		m.setW22Level3(measure.getW22Level3());
		return measuresRepository.save(m);
		
	}

	@Override
	public Boolean deleteAttachment(Integer attachmentId) {
		// TODO Auto-generated method stub
		Attachments a = attachmentsRepository.getOne(attachmentId);
		//System.out.println(a);
		 attachmentsRepository.delete(a);
		 return true;
	}

}
