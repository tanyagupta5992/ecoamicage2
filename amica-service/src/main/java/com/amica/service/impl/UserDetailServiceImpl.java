package com.amica.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amica.exception.NotFoundException;
import com.amica.model.UserDetail;
import com.amica.model.Users;
import com.amica.repository.UserDetailsRepository;
import com.amica.repository.UserRepository;
import com.amica.response.AssignedPropertyProjection;
import com.amica.service.UserDetailService;

@Service
public class UserDetailServiceImpl implements UserDetailService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Override
	public UserDetail saveUserProperty(long userId, UserDetail userDetail) {

		List<UserDetail> userDetails = new ArrayList<>();
		Users user = new Users();

		Optional<Users> byId = userRepository.findById(userId);
		if (!byId.isPresent()) {
			throw new NotFoundException("User not found " + userId);
		}
		Users users = byId.get();
		userDetail.setUsers(users);
		UserDetail detail = userDetailsRepository.save(userDetail);
		userDetails.add(detail);
		user.setUserDetails(userDetails);

		return detail;
	}

	@Override
	public List<AssignedPropertyProjection> findAssignedProperty(String email) {
		List<AssignedPropertyProjection> findByUserEmail = userDetailsRepository.findByEmail(email);
		if(findByUserEmail.isEmpty()) {
			throw new NotFoundException("User not found with " + email + " id");
		}
		return findByUserEmail;
	}

}
