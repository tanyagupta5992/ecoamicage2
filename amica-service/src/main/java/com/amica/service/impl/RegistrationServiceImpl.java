package com.amica.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amica.model.Users;
import com.amica.request.RegistrationRequest;
import com.amica.service.RegistrationService;
import com.amica.service.UserService;

@Service
public class RegistrationServiceImpl implements RegistrationService {
	
	@Autowired
	private UserService UserService;

	@Override
	public String register(RegistrationRequest request) {
			
		String signUpUser = UserService.signUpUser(new Users(
				request.getFirstName(),
				request.getLastName(),
				request.getEmail(),
				request.getPassword(),
				request.getAppUserRole()));;
		return signUpUser;
	}

}
