import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { GreenmeasuresComponent } from './greenmeasures/greenmeasures.component'; 
import { DropdownModule, CheckboxModule, CarouselModule, DialogModule, FileUploadModule, TabViewModule, MenubarModule, PanelMenuModule, ListboxModule, OverlayPanelModule, MultiSelectModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApprovalsComponent } from './approvals/approvals.component';
import { HttpClientModule } from '@angular/common/http';
import { ConsumptionDataComponent } from './consumption-data/consumption-data.component';
import { FooterComponent } from './footer/footer.component';
import { PlasticRecyclingComponent } from './plastic-recycling/plastic-recycling.component';
import { ReportsComponent } from './reports/reports.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    GreenmeasuresComponent,
    ApprovalsComponent,
    ConsumptionDataComponent,
    FooterComponent,
    PlasticRecyclingComponent,
    ReportsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    CheckboxModule,
    DropdownModule,
    CarouselModule,
    DialogModule,
    BrowserAnimationsModule,
    FileUploadModule,
    TabViewModule,
    MenubarModule,
    PanelMenuModule,
    ListboxModule,
    OverlayPanelModule,
    MultiSelectModule,
    CalendarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
