import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  nav(item) {
    if(item=='1') {
      document.getElementById('nav1').style.fontWeight='bold';
      document.getElementById('nav3').style.fontWeight='normal';
      document.getElementById('nav5').style.fontWeight='normal';
      document.getElementById('nav2').style.fontWeight='normal';
      document.getElementById('nav6').style.fontWeight='normal';
      document.getElementById('nav4').style.fontWeight='normal';
    }
    else if(item=='2') {
      document.getElementById('nav1').style.fontWeight='normal';
      document.getElementById('nav3').style.fontWeight='normal';
      document.getElementById('nav5').style.fontWeight='normal';
      document.getElementById('nav2').style.fontWeight='bold';
      document.getElementById('nav6').style.fontWeight='normal';
      document.getElementById('nav4').style.fontWeight='normal';
    }
    else if(item=='3') {
      document.getElementById('nav1').style.fontWeight='normal';
      document.getElementById('nav3').style.fontWeight='bold';
      document.getElementById('nav5').style.fontWeight='normal';
      document.getElementById('nav2').style.fontWeight='normal';
      document.getElementById('nav6').style.fontWeight='normal';
      document.getElementById('nav4').style.fontWeight='normal';
    }
    else if(item=='5') {
      document.getElementById('nav1').style.fontWeight='normal';
      document.getElementById('nav3').style.fontWeight='normal';
      document.getElementById('nav5').style.fontWeight='bold';
      document.getElementById('nav2').style.fontWeight='normal';
      document.getElementById('nav6').style.fontWeight='normal';
      document.getElementById('nav4').style.fontWeight='normal';
    }
    else if(item=='6') {
      document.getElementById('nav1').style.fontWeight='normal';
      document.getElementById('nav3').style.fontWeight='normal';
      document.getElementById('nav5').style.fontWeight='normal';
      document.getElementById('nav2').style.fontWeight='normal';
      document.getElementById('nav6').style.fontWeight='bold';
      document.getElementById('nav4').style.fontWeight='normal';
    }
    else if(item=='4') {
      document.getElementById('nav1').style.fontWeight='normal';
      document.getElementById('nav3').style.fontWeight='normal';
      document.getElementById('nav5').style.fontWeight='normal';
      document.getElementById('nav2').style.fontWeight='normal';
      document.getElementById('nav6').style.fontWeight='normal';
      document.getElementById('nav4').style.fontWeight='bold';
    }
    else {}
  }

}
