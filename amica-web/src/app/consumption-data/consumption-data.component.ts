import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APIConfig } from '../../config';

@Component({
  selector: 'app-consumption-data',
  templateUrl: './consumption-data.component.html',
  styleUrls: ['./consumption-data.component.css']
})
export class ConsumptionDataComponent implements OnInit {

  constructor(private http:HttpClient) { }

  utilitySelected='Electricity';
  utilityId=1;
  dateValue=new Date();
  monthSelected:any;
  yearSelected:any;
  usageData:any;
  usageText=null;
  usageId=0;
  today=new Date();
  submitData={
    month: '',
    usageId: null,
    usageText: null,
    utilityType: '',
    year: ''
  }
  showSaveDialog:boolean;
  emissionFactors:any;
  emissionFactorsAvailable:boolean;
  prevUsageText=null;
  
  ngOnInit() {
    this.getUsageData();
  }

  utilityChosen(utility,id) {
    this.utilitySelected=utility;
    this.utilityId=id;
    this.getUsageData();
    document.getElementById('electricity').style.fontWeight='normal';
    document.getElementById('water').style.fontWeight='normal';
    document.getElementById('gas').style.fontWeight='normal';
    document.getElementById('plastic').style.fontWeight='normal';
    let formattedUtility = utility.toLowerCase();
    document.getElementById(''+formattedUtility).style.fontWeight='bold';
  }

  getUsageData() {
    this.getEmissionFactors();
    this.usageData=null;
    this.usageText=null;
    this.prevUsageText=null;
    this.monthSelected = this.dateValue.getMonth();
    this.yearSelected = this.dateValue.getFullYear();
    this.http.get(APIConfig.API_ENDPOINT+'/service/getUsage/'+this.monthSelected+'/'+this.yearSelected+'/'+APIConfig.HOTEL_CODE)
    .subscribe(res => {
      this.usageData=res;
      this.usageData.forEach((element,index) => {
        if(element.utilityType == this.utilitySelected) {
          this.usageText=element.usageText;
          this.usageId=element.usageId;
          this.prevUsageText = element.usageText;
        }
      })
    })
  }

  getEmissionFactors() {
    this.emissionFactorsAvailable=false;
    this.yearSelected = this.dateValue.getFullYear();
    this.http.get(APIConfig.API_ENDPOINT+'/service/getEmmissionFactors/'+APIConfig.HOTEL_CODE+'/'+this.yearSelected+'/'+this.utilityId)
    .subscribe(res => {
      this.emissionFactors=res;
      this.emissionFactorsAvailable=true;
    })
  }

  save() {
    console.log(this.prevUsageText);
    if(this.prevUsageText == null) {
      this.submitData={
        month: ''+this.monthSelected,
        usageId: null,
        usageText: this.usageText,
        utilityType: this.utilitySelected,
        year: ''+this.yearSelected
      }
      this.http.post(APIConfig.API_ENDPOINT+'/service/save/usage/'+this.utilityId+'/'+APIConfig.HOTEL_CODE,this.submitData)
      .subscribe(res => {
         this.getUsageData();
         this.showSaveDialog=true;
      })
    }  
    else {
      this.submitData={
        month: this.monthSelected,
        usageId: this.usageId,
        usageText: parseInt(this.usageText),
        utilityType: this.utilitySelected,
        year: ''+this.yearSelected
      }
      let month = this.monthSelected;
      this.http.put(APIConfig.API_ENDPOINT+'/service/update/usage',this.submitData)
      .subscribe(res => {
         this.getUsageData();
         this.showSaveDialog=true;
      })
    }
  }

  closeSaveDialog() {
    this.showSaveDialog=false;
  }

}
