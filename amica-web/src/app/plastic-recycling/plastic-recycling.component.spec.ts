import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlasticRecyclingComponent } from './plastic-recycling.component';

describe('PlasticRecyclingComponent', () => {
  let component: PlasticRecyclingComponent;
  let fixture: ComponentFixture<PlasticRecyclingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlasticRecyclingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlasticRecyclingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
