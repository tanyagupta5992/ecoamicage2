import { Component, OnInit } from '@angular/core';
import * as Highcharts from "highcharts";
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
import Exporting from 'highcharts/modules/exporting';
Exporting(Highcharts);
import { HttpClient } from '@angular/common/http';
import { APIConfig } from '../../config';

@Component({
  selector: 'app-plastic-recycling',
  templateUrl: './plastic-recycling.component.html',
  styleUrls: ['./plastic-recycling.component.css']
})
export class PlasticRecyclingComponent implements OnInit {

  agencyList:any;
  yesNoList:any;
  dateValue=new Date();
  usageData:any;
  usageText=null;
  usageId=0;
  prevUsageText=null;
  monthSelected:any;
  yearSelected:any;
  showSaveDialog:boolean;
  recycleData:any;
  plasticYTDData:any;
  monthwiseConsumptionData = [];
  monthwiseRecycleData = [];
  isCollaborating:boolean;
  constructor(private http:HttpClient) { }
 
  ngOnInit() {
    this.getRecycleInfo();
    this.getPlasticYTDData();
    this.recycleData = {
      isCollaborating: '',
      collaboratingAgency: '',
      hotelCode: APIConfig.HOTEL_CODE
    }
    this.agencyList=[
      {label:'Agency A',value:'Agency A'},
      {label:'Agency B',value:'Agency B'},
      {label:'Agency C',value:'Agency C'},
      {label:'Agency D',value:'Agency D'}
    ]
    this.yesNoList=[
      {label:'Yes',value:'Yes'},
      {label:'No',value:'No'}
    ]
    this.getUsageData();  
  }  

  generateChart() {
    var chart1 = Highcharts.chart({
      chart: {
          renderTo: 'container',
          type: 'pie'
      },
      title: {
          text: 'Plastic Consumption/Recycling YTD'
      },
      credits: {
        enabled:false
      },
      xAxis: {
         categories: ['January', 'February', 'March', 'April', 'May','June','July','August','September','October','November','December'],
         title: {
           text: 'Months'
         }
       },
       yAxis: {
         min: 0,
         title: {
           text: 'Consumption (kgs)',
           align: 'high'
         },
         labels: {
           overflow: 'justify'
         }
       },
      tooltip: {
        pointFormat: '<b>{point.y:.1f}kgs</b>'
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
      plotOptions: {
        pie: {
            allowPointSelect: false,
            cursor: 'pointer',
            dataLabels: {
                enabled: false,
                format: '<b>{point.name}</b>: {point.y:.1f} kgs'
            }
        }
      },
      series: [{
          name: 'Plastic Recycled',
          colorByPoint: true,
          colors: ['lightgreen','green'], 
          type: 'pie',
          data: [{
              name: 'Pastic Consumed',
              y: this.plasticYTDData.totalConsumption,
              drilldown: 'Plastic Consumed Monthly'
          }, {
              name: 'Plastic Recycled',
              y: this.plasticYTDData.totalPlasticRecycled,
              drilldown: 'Plastic Recycled Monthly'
          }]
      }],
      drilldown: {
        series: [{
            name: 'Plastic Consumed Monthly',
            id: 'Plastic Consumed Monthly',
            type:'column',
            data: this.monthwiseConsumptionData
        }, {
            name: 'Plastic Recycled Monthly',
            id: 'Plastic Recycled Monthly',
            type: 'column',
            data: this.monthwiseRecycleData
        }]
      }
    });
  }

  getPlasticYTDData() {
    this.monthwiseConsumptionData=[];
    this.monthwiseRecycleData=[];
    let consumptionMonths = [];
    let recycleMonths = [];
    let year=this.dateValue.getFullYear();
    this.http.get(APIConfig.API_ENDPOINT+'/service/getPlasticData/'+APIConfig.HOTEL_CODE+'/'+year)
      .subscribe(res => {
         this.plasticYTDData=res;
         for(let element in this.plasticYTDData.usageData) {
            consumptionMonths.push(element);
         }
         for(let element in this.plasticYTDData.usageDataRec) {
            recycleMonths.push(element);
         }
         for(let element in this.plasticYTDData.usageData) {
           switch (element) {
             case '0':
                this.monthwiseConsumptionData.push(['January',this.plasticYTDData.usageData[element]]);
                break;
             case '1':
                this.monthwiseConsumptionData.push(['February',this.plasticYTDData.usageData[element]]);
                break;   
             case '2':
                this.monthwiseConsumptionData.push(['March',this.plasticYTDData.usageData[element]]);
                break;
             case '3':
                this.monthwiseConsumptionData.push(['April',this.plasticYTDData.usageData[element]]);
                break;
             case '4':
                this.monthwiseConsumptionData.push(['May',this.plasticYTDData.usageData[element]]);
                break;   
             case '5':
                this.monthwiseConsumptionData.push(['June',this.plasticYTDData.usageData[element]]);
                break;
             case '6':
                this.monthwiseConsumptionData.push(['July',this.plasticYTDData.usageData[element]]);
                break;
             case '7':
                this.monthwiseConsumptionData.push(['August',this.plasticYTDData.usageData[element]]);
                break;   
             case '8':
                this.monthwiseConsumptionData.push(['September',this.plasticYTDData.usageData[element]]);
                break;
             case '9':
                this.monthwiseConsumptionData.push(['October',this.plasticYTDData.usageData[element]]);
                break;
             case '10':
                this.monthwiseConsumptionData.push(['November',this.plasticYTDData.usageData[element]]);
                break;   
             case '11':
                this.monthwiseConsumptionData.push(['December',this.plasticYTDData.usageData[element]]);
                break;                               
           }
         }
         for(let element in this.plasticYTDData.usageDataRec) {
          switch (element) {
            case '0':
               this.monthwiseRecycleData.push(['January',this.plasticYTDData.usageDataRec[element]]);
               break;
            case '1':
               this.monthwiseRecycleData.push(['February',this.plasticYTDData.usageDataRec[element]]);
               break;   
            case '2':
               this.monthwiseRecycleData.push(['March',this.plasticYTDData.usageDataRec[element]]);
               break;
            case '3':
               this.monthwiseRecycleData.push(['April',this.plasticYTDData.usageDataRec[element]]);
               break;
            case '4':
               this.monthwiseRecycleData.push(['May',this.plasticYTDData.usageDataRec[element]]);
               break;   
            case '5':
               this.monthwiseRecycleData.push(['June',this.plasticYTDData.usageDataRec[element]]);
               break;
            case '6':
               this.monthwiseRecycleData.push(['July',this.plasticYTDData.usageDataRec[element]]);
               break;
            case '7':
               this.monthwiseRecycleData.push(['August',this.plasticYTDData.usageDataRec[element]]);
               break;   
            case '8':
               this.monthwiseRecycleData.push(['September',this.plasticYTDData.usageDataRec[element]]);
               break;
            case '9':
               this.monthwiseRecycleData.push(['October',this.plasticYTDData.usageDataRec[element]]);
               break;
            case '10':
               this.monthwiseRecycleData.push(['November',this.plasticYTDData.usageDataRec[element]]);
               break;   
            case '11':
               this.monthwiseRecycleData.push(['December',this.plasticYTDData.usageDataRec[element]]);
               break;                               
          }
         }
         if(this.plasticYTDData.totalConsumption != 0) {
            this.generateChart();
         }         
      })
  }

  saveRecycleInfo() {
    this.http.post(APIConfig.API_ENDPOINT+'/service/saveRecyclingData',this.recycleData)
      .subscribe(res => {
         this.getUsageData();
         this.showSaveDialog=true;
    })
  }

  getRecycleInfo() {
    this.http.get(APIConfig.API_ENDPOINT+'/service/getPlasticCollabData/'+APIConfig.HOTEL_CODE)
      .subscribe(res => {
         this.recycleData=res;
         if(this.recycleData.isCollaborating=='No') {
            this.isCollaborating=false;
            this.agencyList=[
               {label:'Not Applicable',value:'Not Applicable'}
            ]
         }
         else {
            this.isCollaborating=true;
         }
    })
  }

  collabChange() {
     if(this.recycleData.isCollaborating=='Yes') {
       this.isCollaborating=true;
       this.agencyList=[
         {label:'Agency A',value:'Agency A'},
         {label:'Agency B',value:'Agency B'},
         {label:'Agency C',value:'Agency C'},
         {label:'Agency D',value:'Agency D'}
       ]
     }
     else {
        this.isCollaborating=false;
        this.agencyList=[
         {label:'Not Applicable',value:'Not Applicable'}
        ]
     }
  }

  getUsageData() {
    this.usageData=null;
    this.usageText=null;
    this.prevUsageText=null;
    this.monthSelected = this.dateValue.getMonth();
    this.yearSelected = this.dateValue.getFullYear();
    this.http.get(APIConfig.API_ENDPOINT+'/service/getUsage/'+this.monthSelected+'/'+this.yearSelected+'/'+APIConfig.HOTEL_CODE)
    .subscribe(res => {
      this.usageData=res;
      this.usageData.forEach((element,index) => {
        if(element.utilityType == 'Plastic Recycled') {
          this.usageText=element.usageText;
          this.usageId=element.usageId;
          this.prevUsageText = element.usageText;
        }
      })
    })
  }
   
  save() {
    this.saveRecycleInfo();
    console.log(this.prevUsageText);
    if(this.prevUsageText == null) {
      let submitData={
        month: ''+this.monthSelected,
        usageId: null,
        usageText: this.usageText,
        utilityType: 'Plastic Recycled',
        year: ''+this.yearSelected
      }
      this.http.post(APIConfig.API_ENDPOINT+'/service/save/usage/5/'+APIConfig.HOTEL_CODE,submitData)
      .subscribe(res => {
         this.getUsageData();
         this.getRecycleInfo();
         this.getPlasticYTDData();
         this.showSaveDialog=true;
      })
    }  
    else {
      let submitData={
        month: this.monthSelected,
        usageId: this.usageId,
        usageText: parseInt(this.usageText),
        utilityType: 'Plastic Recycled',
        year: ''+this.yearSelected
      }
      let month = this.monthSelected;
      this.http.put(APIConfig.API_ENDPOINT+'/service/update/usage',submitData)
      .subscribe(res => {
         this.getUsageData();
         this.getRecycleInfo();
         this.getPlasticYTDData();
         this.showSaveDialog=true;
      })
    }
  }

  closeSaveDialog() {
    this.showSaveDialog=false;
  }

}
