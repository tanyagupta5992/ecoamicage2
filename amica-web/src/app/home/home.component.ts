import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import * as Highcharts from "highcharts";
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
import Exporting from 'highcharts/modules/exporting';
Exporting(Highcharts);
import { APIConfig } from '../../config';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private http:HttpClient) { }
  tips:any;
  tipsAvailable:boolean;
  carbonData:any;
  year:any;
  prevYear:any;
  areaColor:any;
  areaColorPlastic:any;
  isHotelAvailable:boolean;
  hotelData:any;
  rankingData:any;
  rankingDataForChart=[];
  rankingHotels=[];
  viewModal:boolean;

  ngOnInit() {
    this.year = new Date().getFullYear();
    this.prevYear=this.year-1;
    this.getTopTips();
    this.getCarbonData();
    this.getHotelDetails();
  }

  showTopTips() {
    this.viewModal=true;
  }

  hideTopTips() {
    this.viewModal=false;
  }

  getHotelDetails() {
    this.isHotelAvailable=false;
    this.http.get(APIConfig.API_ENDPOINT+'/service/getHotelDetails/'+APIConfig.HOTEL_CODE)
    .subscribe(res => {
       this.hotelData=res;
       this.isHotelAvailable=true;
    })
  }

  getTopTips() {
    this.tipsAvailable=false;
    this.http.get(APIConfig.API_ENDPOINT+'/service/getAllTopTips/'+APIConfig.HOTEL_CODE)
    .subscribe(res => {
      this.tips = res;
      this.tipsAvailable=true;
    })  
  }

  getCarbonData() {
    this.http.get(APIConfig.API_ENDPOINT+'/service/getConsumptionForHotelByYear/'+APIConfig.HOTEL_CODE+'/'+this.year)
    .subscribe(res => {
      this.carbonData = res;
      console.log(parseInt(this.carbonData.variance));
      if(parseInt(this.carbonData.variance) > 0) {
        this.areaColor='crimson';
      }
      else {
        this.areaColor='green';
      }
      if(parseInt(this.carbonData.plasticVariance) > 0) {
        this.areaColorPlastic='crimson';
      }
      else {
        this.areaColorPlastic='green';
      }
      this.generateCarbonChart();
      this.generatePlasticChart();
      this.getRankingData();
    })
  }

  generateCarbonChart() {
    var chart1 = Highcharts.chart({
      chart: {
          renderTo: 'container',
          type: 'line',
          height:325,
          width:820
      },
      title: {
          text: 'Carbon YTD Variance'
      },
      credits: {
        enabled:false
      },
      exporting: {
        enabled:true
      },
      xAxis: {
         categories: [this.year-1, this.year],
         title: {
           text: 'Year'
         },
         labels: {
          style: {
            fontWeight:'bold'
          }
         }
       },
       yAxis: {
         min: 0,
         title: {
           text: 'Total Carbon Consumption (kwh)',
           align: 'high'
         },
         labels: {
           overflow: 'justify',
           style: {
            fontWeight:'bold'
          }
         }
       },
      tooltip: {
        pointFormat: '<b>{point.y:.1f} kwh </b>'
      },
      plotOptions: {
        area: {
          dataLabels: {
            enabled: true,
            formatter: function() {
                return Math.round(this.point.y)+' kwh'
            }
          }
        }
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
      series: [
        {
          name: 'Total Carbon',
          type:'area',
          color:this.areaColor,
          data: [this.carbonData.carbonConsumptionLastYear,this.carbonData.carbonConsumption]
        }]  
    });
  }

  generatePlasticChart() {
    var chart1 = Highcharts.chart({
      chart: {
          renderTo: 'container2',
          type: 'line',
          height:325,
          width:820
      },
      title: {
          text: 'Plastic YTD Variance'
      },
      credits: {
        enabled:false
      },
      xAxis: {
         categories: [this.year-1, this.year],
         title: {
           text: 'Year'
         },
         labels: {
          style: {
            fontWeight:'bold'
          }
         }
       },
       yAxis: {
         min: 0,
         title: {
           text: 'Total Plastic Consumption (kgs)',
           align: 'high'
         },
         labels: {
           overflow: 'justify',
           style: {
            fontWeight:'bold'
          }
         }
       },
       plotOptions: {
        area: {
          dataLabels: {
            enabled: true,
            formatter: function() {
              return Math.round(this.point.y)+' kgs'
            }
          }
        }
      },
      tooltip: {
        pointFormat: '<b>{point.y:.1f} kgs </b>'
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
      series: [
        {
          name: 'Total Plastic',
          type:'area',
          color:this.areaColorPlastic,
          data: [this.carbonData.plasticConsumptionLastYear,this.carbonData.plasticConsumption]
        }]  
    });
  }

  getRankingData() {
     this.rankingDataForChart=[];
     this.rankingHotels=[];
     this.http.get(APIConfig.API_ENDPOINT+'/service/getRegionBrandConsumption/'+APIConfig.HOTEL_CODE+'/'+this.year)
     .subscribe(res => {
         this.rankingData=res;
         let i=0;
         for(let hotel of this.rankingData) {
           i++;
           this.rankingHotels.push(hotel.hotelCode);
           this.rankingDataForChart.push(hotel.carbonConsumption);
         }
        this.generateRankingChart();
     })
  }

  generateRankingChart() {
    var chart3 = Highcharts.chart({
      chart: {
        type: 'column',
        renderTo: 'container3',
        height: 325,
        width: 800
      },
      title: {
        text: 'Your Carbon Ranking'
      },
      xAxis: {
        categories: this.rankingHotels,
        labels: {
           style: {
             fontWeight:'bold'
           }
        },
        title: {
          text: 'Hotels'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total Carbon Consumption (Kwh)',
          align: 'high'
        },
        labels: {
          overflow: 'justify',
          style: {
            fontWeight:'bold'
          }
        }
      },
      tooltip: {
        valueSuffix: 'kwh'
      },
      plotOptions: {
        column: {
          dataLabels: {
            enabled: true,
            formatter: function () {
              let rank = this.point.index;
              ++rank;
              return 'Rank '+rank;
            }
          }
        }
      },
      legend: {
        layout: 'horizontal',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: -10,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Total Carbon Consumption',
        type:'column',
        data: this.rankingDataForChart
      }]
    });
  }

}
