import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Highcharts from "highcharts";
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
import Exporting from 'highcharts/modules/exporting';
Exporting(Highcharts);
import { APIConfig } from '../../config';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['../consumption-data/consumption-data.component.css']
})
export class ReportsComponent implements OnInit {

  utilitySelected='Electricity';
  utilityId=1;
  uom='kwh';
  dateValue=new Date();
  monthSelected:any;
  yearSelected:any;
  prevYear:any;
  currentReportData:any;
  prevReportData:any;
  finalCurrentData:any;
  finalPrevData:any;

  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.yearSelected = this.dateValue.getFullYear();
    this.prevYear=this.yearSelected-1;
    this.getReportData();
  }

  getReportData() {
    this.yearSelected = this.dateValue.getFullYear();
    this.prevYear = this.yearSelected-1;
    this.finalCurrentData=[];
    this.finalPrevData=[];
    this.http.get(APIConfig.API_ENDPOINT+'/service/getReportData/'+APIConfig.HOTEL_CODE+'/'+this.yearSelected+'/'+this.utilityId)
    .subscribe(res => {
      this.currentReportData = res;
      this.http.get(APIConfig.API_ENDPOINT+'/service/getReportData/'+APIConfig.HOTEL_CODE+'/'+this.prevYear+'/'+this.utilityId)
        .subscribe(res => {
          this.prevReportData = res;
          this.generateChart();
        })
    })
  }

  generateChart() {
    var chart1 = Highcharts.chart({
      chart: {
        type: 'column',
        renderTo: 'container',
        height: 350,
        width: 900
      },
      title: {
        text: this.utilitySelected+' Consumption Report'
      },
      xAxis: {
        categories: ['January', 'February', 'March', 'April', 'May','June','July','August','September','October','November','December'],
        title: {
          text: 'Months'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Consumption ('+this.uom+')',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: this.uom
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'horizontal',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: -10,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        name: ''+this.yearSelected,
        type:'column',
        data: [this.currentReportData.usageData[0],
               this.currentReportData.usageData[1],
               this.currentReportData.usageData[2],
               this.currentReportData.usageData[3],
               this.currentReportData.usageData[4],
               this.currentReportData.usageData[5],
               this.currentReportData.usageData[6],
               this.currentReportData.usageData[7],
               this.currentReportData.usageData[8],
               this.currentReportData.usageData[9],
               this.currentReportData.usageData[10],
               this.currentReportData.usageData[11]]
      }, {
        name: ''+this.prevYear,
        type:'column',
        data: [this.prevReportData.usageData[0],
              this.prevReportData.usageData[1],
              this.prevReportData.usageData[2],
              this.prevReportData.usageData[3],
              this.prevReportData.usageData[4],
              this.prevReportData.usageData[5],
              this.prevReportData.usageData[6],
              this.prevReportData.usageData[7],
              this.prevReportData.usageData[8],
              this.prevReportData.usageData[9],
              this.prevReportData.usageData[10],
              this.prevReportData.usageData[11]]
      }]
    });
  }

  utilityChosen(utility,id) {
    this.utilitySelected=utility;
    this.utilityId=id;
    this.getReportData();
    if(utility == 'Electricity' || utility == 'Gas') {
      this.uom = 'kwh';
    }
    else if(utility == 'Water') {
      this.uom = 'gallons'
    }
    else if(utility == 'Plastic') {
      this.uom = 'kgs'
    }
    else {}
    document.getElementById('electricity').style.fontWeight='normal';
    document.getElementById('water').style.fontWeight='normal';
    document.getElementById('gas').style.fontWeight='normal';
    document.getElementById('plastic').style.fontWeight='normal';
    let formattedUtility = utility.toLowerCase();
    document.getElementById(''+formattedUtility).style.fontWeight='bold';
  }

}
