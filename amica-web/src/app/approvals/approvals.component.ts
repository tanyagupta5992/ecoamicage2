import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { APIConfig } from '../../config'; 

@Component({
  selector: 'app-approvals',
  templateUrl: './approvals.component.html',
  styleUrls: ['./approvals.component.css']
})
export class ApprovalsComponent implements OnInit {

  constructor(private http:HttpClient) { }

  proofList:any;
  newList:any;
  hasProofs:boolean;
  showConfirmDialog:boolean;
  status='';

  ngOnInit() {
    this.getProofs();
  }

  getProofs() {
    this.hasProofs=false;
    this.newList=[];
    this.http.get(APIConfig.API_ENDPOINT+'/service/getFilesForReview')
    .subscribe(res => {
      this.proofList=res;
      for(let proof of this.proofList) {
        if(proof.status != 'Approved' && proof.status != 'Rejected') {
          this.newList.push(proof);
        }
      }
      if(this.newList.length>0) {
        this.hasProofs=true;
      }
    })
  }

  downloadFile(attachment) {
    window.open(APIConfig.API_ENDPOINT+'/service/downloadFile/'+attachment.attachment_id,attachment.fileName);
  }

  update(status,id) {
    let submitData={
      attachmentId:id,
      updatedStatus:status
    }
    this.http.post(APIConfig.API_ENDPOINT+'/service/updateStatus/'+id+'/'+status,submitData)
    .subscribe(res => {
      this.getProofs();
      if(status=='Approved') {
        this.status = 'Approval Successful!';
      }
      else {
        this.status = 'Rejection Successful!';
      }
      this.showConfirmDialog=true;
    })
  }

  hideFileConfirmDialog() {
    this.showConfirmDialog=false;
  }

}
