import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GreenmeasuresComponent } from './greenmeasures.component';

describe('GreenmeasuresComponent', () => {
  let component: GreenmeasuresComponent;
  let fixture: ComponentFixture<GreenmeasuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GreenmeasuresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GreenmeasuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
