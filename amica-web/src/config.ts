
// comment 1st config block and uncomment 2nd config block for AWS Deployment 

export const APIConfig = {
    API_ENDPOINT: "http://localhost:8080/amica-app",
    HOTEL_CODE: "TESTA"
};

// comment 2nd config block and uncomment 1st config block for Local Deployment 

/* export const APIConfig = {
     API_ENDPOINT: "http://ecoamica-env.eba-bfrp45gy.ap-south-1.elasticbeanstalk.com",
     HOTEL_CODE: "TESTA"
}; */